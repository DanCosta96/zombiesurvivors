using NUnit.Framework;
using System;

[TestFixture]
public class GameTests
{
    [Test]
    public void GameBeginsWith0Survivors()
    {
        var game = new Game();

        Assert.That(game.Survivors.Count, Is.EqualTo(0));
    }

    [Test]
    public void GameBeginsWithLevelBlue()
    {
        var game = new Game();

        Assert.That(game.GameLevel, Is.EqualTo(Level.Blue));
    }

    [Test]
    public void GameBeginsHasHistory()
    {
        var game = new Game();

        Assert.That(game.GameHistory, Is.Not.EqualTo(null));
    }

    [Test]
    public void GameRecordAtBeginTheTimeOfGameBegan()
    {
        string BeginingTime = DateTime.Now.ToString("h:mm:ss");
        var game = new Game();
        Assert.That(game.GameHistory.RecordedEvents[0], Is.EqualTo("Game Began at: " + BeginingTime));
    }

    [Test]
    public void SurvivorCanBeAdded()
    {
        var game = new Game();

        game.AddSurvivor("Joe");

        Assert.That(game.Survivors.Count, Is.EqualTo(1));
    }

    [Test]
    public void GameRecordWhenASurvivorIsAdded()
    {
        var game = new Game();

        game.AddSurvivor("Jimmy");

        Assert.That(game.GameHistory.RecordedEvents[game.GameHistory.RecordedEvents.Count-1],Is.EqualTo("The Survivor Jimmy has been added to the game."));
    }

    [Test]
    public void GameRecordWhenASurvivorAcquireAnEquipment()
    {
        var game = new Game();

        game.AddSurvivor("Jimmy").AddEquipment(new Equipment("Dagger"));

        Assert.That(game.GameHistory.RecordedEvents[game.GameHistory.RecordedEvents.Count - 1], Is.EqualTo("The Survivor Jimmy acquire: Dagger."));
    }

    [Test]
    public void GameRecordWhenASurvivorReceiveWound()
    {
        var game = new Game();

        game.AddSurvivor("Jimmy").ReceiveWounds(1);

        Assert.That(game.GameHistory.RecordedEvents[game.GameHistory.RecordedEvents.Count - 1], Is.EqualTo("The Survivor Jimmy receive 1 wound."));
    }

    [Test]
    public void GameRecordWhenASurvivorDead()
    {
        var game = new Game();

        game.AddSurvivor("Jimmy").ReceiveWounds(2);

        Assert.That(game.GameHistory.RecordedEvents[3], Is.EqualTo("The Survivor Jimmy die."));
    }

    [Test]
    public void GameRecordWhenASurvivorLevelUp()
    {
        var game = new Game();

        game.AddSurvivor("Jimmy").GainExperience(10);

        Assert.That(game.GameHistory.RecordedEvents[2], Is.EqualTo("The Survivor Jimmy Reach Level Yellow."));
    }

    [Test]
    public void GameRecordWhenTheGameLevelUp()
    {
        var game = new Game();
        
        game.AddSurvivor("Repe").GainExperience(43);

        Assert.That(game.GameHistory.RecordedEvents[3], Is.EqualTo("The Game is now Level Red."));
    }

    [Test]
    public void GameRecordWhenGameEnd()
    {
        var game = new Game();

        game.AddSurvivor("Repe").ReceiveWounds(2);
        game.AddSurvivor("Gil").ReceiveWounds(2);
        game.AddSurvivor("Bert").ReceiveWounds(2);

        Assert.That(game.GameHistory.RecordedEvents[game.GameHistory.RecordedEvents.Count - 1], Is.EqualTo("The Game has ended."));
    }

    [Test]
    public void SurvivorsCannotHaveTheSameName()
    {
        var game = new Game();

        game.AddSurvivor("Joe");

        Assert.Throws<ArgumentException>(() => game.AddSurvivor("Joe"));
        Assert.That(game.Survivors.Count, Is.EqualTo(1));
    }

    [Test]
    public void GameEndIfAreSurvivorsAreDead()
    {
        var game = new Game();

        game.AddSurvivor("Joe");
        game.AddSurvivor("Rick");

        game.Survivors[0].IsDead = true;
        game.Survivors[1].IsDead = true;

        Assert.That(game.Ended(), Is.EqualTo(true));
    }

    [Test]
    [TestCase(11,50,32,Level.Red)]
    [TestCase(41,4,3,Level.Orange)]
    [TestCase(12,14,18,Level.Yellow)]
    public void GameLevelIsEqualToHighestSurvivorLevel(
        int survivor0Experience,
        int survivor1Experience, 
        int survivor2xperience,
        Level expectedLevel)
    {
        var game = new Game();
        game.AddSurvivor("Joe");
        game.AddSurvivor("Rick");
        game.AddSurvivor("Zed");

        game.Survivors[0].GainExperience(survivor0Experience);
        game.Survivors[1].GainExperience(survivor1Experience);
        game.Survivors[2].GainExperience(survivor2xperience);

        Assert.That(game.GameLevel, Is.EqualTo(expectedLevel));
    }
}
