using System;
using System.Collections.Generic;

public class Survivor
{
    public string Name;
    public int Wounds = 0;
    public bool IsDead = false;
    public int ActionsPerTurn = 3;
    public int MaxEquipment = 5;
    public int Experience = 0;
    public Level CurrentLevel = Level.Blue;

    public List<Equipment> InReserve = new List<Equipment>();
    public List<Equipment> InHand = new List<Equipment>();

    public static event Action<string, string> OnAddEquipment;
    public static event Action<string, int> OnWounded;
    public static event Action<string> OnDie;
    public static event Action<string, Level> OnLevelUp;

    public Survivor(string name)
    {
        this.Name = name;
    }

    public void ReceiveWounds(int amount)
    {
        if (amount > 2) amount = 2;
        OnWounded?.Invoke(Name, amount);
        Wounds += amount;
        if (Wounds >= 2)
        {
            Wounds = 2;
            IsDead = true;
            OnDie?.Invoke(Name);
        }
        MaxEquipment -= Wounds;
        DiscardEquipmentToMatchMaxEquipment();

    }

    public void AddEquipment(Equipment equipment)
    {
        if (MaxEquipmentReached())
            return;
        InReserve.Add(equipment);
        OnAddEquipment?.Invoke(Name, equipment.Name);
    }
    public void MoveToHand(Equipment equipment)
    {
        if (InHand.Count >= 2)
            return;
        if (!InReserve.Contains(equipment))
            return;
        InHand.Add(equipment);
        InReserve.Remove(equipment);
    }

    public bool MaxEquipmentReached()
    {
        return TotalEquipmentInPosesion() >= MaxEquipment;
    }

    public int TotalEquipmentInPosesion()
    {
        return InHand.Count + InReserve.Count;
    }

    public void DiscardEquipmentToMatchMaxEquipment()
    {
        int EquipmentsToDiscard = TotalEquipmentInPosesion() - MaxEquipment;
        if (EquipmentsToDiscard <= 0) return;
        for (int i = 0; i < EquipmentsToDiscard; i++)
        {
            if (InReserve.Count > 0)
            {
                InReserve.RemoveAt(InReserve.Count - 1);
            }
            else if (InHand.Count > 0)
            {
                InHand.RemoveAt(InHand.Count - 1);
            }
        }
       
    }

    public void KillZombie()
    {
        GainExperience(1);
    }

    public void GainExperience(int amount)
    {
        Experience += amount;
        HandleLevelUp();
    }

    public void HandleLevelUp()
    {
        if (Experience > 42)
        {
            if (CurrentLevel != Level.Red)
            {
                CurrentLevel = Level.Red;
                OnLevelUp?.Invoke(Name, CurrentLevel);
            }
        }
        else if (Experience > 18 )
        {
            if (CurrentLevel != Level.Orange)
            {
                CurrentLevel = Level.Orange;
                OnLevelUp?.Invoke(Name, CurrentLevel);
            }
        }
        else if (Experience > 6)
        {
            if (CurrentLevel != Level.Yellow)
            {
                CurrentLevel = Level.Yellow;
                OnLevelUp?.Invoke(Name, CurrentLevel);
            }
        }
        else
        {
            CurrentLevel = Level.Blue;
        }
    }
}