using System;
using System.Collections.Generic;

public class Game
{
    public List<Survivor> Survivors = new List<Survivor>();
    public History GameHistory = new History();

    private Level previousLevel = Level.Blue;
    public Level GameLevel
    {
        get { return GetGameLevel(); }
    }

    public Game()
    {
        GameHistory.RecordEvent("Game Began at: " + DateTime.Now.ToString("h:mm:ss"));
        Survivor.OnAddEquipment += SurvivorEquipmentLog;
        Survivor.OnWounded += SurvivorWoundedLog;
        Survivor.OnDie += SurvivorDiedLog;
        Survivor.OnLevelUp += SurvivorLevelUpLog;
    }

    public Survivor AddSurvivor(string name)
    {
        foreach (Survivor survivor in Survivors)
        {
            if (survivor.Name == name)
            {
                throw new ArgumentException("Survivors should have an Unique name");
            }
        }
        Survivor survivorToAdd = new Survivor(name);
        Survivors.Add(survivorToAdd);
        GameHistory.RecordEvent("The Survivor " + name + " has been added to the game.");
        return survivorToAdd;
    }

    public bool Ended()
    {
        if (Survivors.Count == 0)
            return false;

        foreach (var survivor in Survivors)
        {
            if (!survivor.IsDead)
                return false;
        }
        return true;
    }
    private Level GetGameLevel()
    {
        if (Survivors.Count == 0)
            return Level.Blue;
        else
            return GetMaxSurvivorLevel();

    }

    public Level GetMaxSurvivorLevel()
    {
        int maxSurvivorExperience = 0;
        Level maxSurvivorLevel = Level.Blue;
        foreach (var survivor in Survivors)
        {
            if (survivor.Experience >= maxSurvivorExperience)
            {
                maxSurvivorExperience = survivor.Experience;
                maxSurvivorLevel = survivor.CurrentLevel;
            }
        }
        return maxSurvivorLevel;
    }

    public void SurvivorEquipmentLog(string name, string equipment)
    {
        GameHistory.RecordEvent("The Survivor "+name+" acquire: "+ equipment +".");
    }

    public void SurvivorWoundedLog(string name, int wounds)
    {
        GameHistory.RecordEvent("The Survivor " + name + " receive " + wounds +  " wound.");
    }

    public void SurvivorDiedLog(string name)
    {
        GameHistory.RecordEvent("The Survivor " + name + " die.");
        if (Ended())
        {
            GameHistory.RecordEvent("The Game has ended.");
        }

    }
    public void SurvivorLevelUpLog(string name, Level level)
    {
        GameHistory.RecordEvent("The Survivor " + name + " Reach Level " + level + ".");
        if (previousLevel != GameLevel)
        {
            GameHistory.RecordEvent("The Game is now Level " + GameLevel + ".");
            previousLevel = GameLevel;
        }
    }

}



public enum Level
{
    Blue,
    Yellow,
    Orange,
    Red
}