using NUnit.Framework;

[TestFixture]
public class SurvivorsTests
{
    public class SurvivorSetUpTests
    {
        [Test]
        [TestCase("Jhon","Jhon")]
        [TestCase("Rick","Rick")]
        public void SurvivorHasName(string settedName, string expectedName)
        {
            var survivor = new Survivor("Jhon");

            Assert.That(survivor.Name, Is.EqualTo("Jhon"));
        }

        [Test]
        public void SurvivorStartWith0Wounds()
        {
            var survivor = new Survivor("Rick");

            Assert.That(survivor.Wounds, Is.EqualTo(0));
        }

        [Test]
        public void SurvivorStartWith3ActionsPerTurn()
        {
            var survivor = new Survivor("Jhon");

            Assert.That(survivor.ActionsPerTurn,Is.EqualTo(3));
        }

        [Test]
        public void SurvivorHasMaxEquipmentOf5()
        {
            var survivor = new Survivor("Josesito");

            Assert.That(survivor.MaxEquipment, Is.EqualTo(5));
        }

        [Test]
        public void SurvivorStartWith0Experiencie()
        {
            var survivor = new Survivor("Veteran");

            Assert.That(survivor.Experience, Is.EqualTo(0));
        }

        [Test]
        public void SurvivorStartWithLevelBlue()
        {
            var survivor = new Survivor("Name");

            Assert.That(survivor.CurrentLevel, Is.EqualTo(Level.Blue));
        }
    }

    public class SurvivorWoundsTests
    {
        [Test]
        [TestCase(1,1)]
        [TestCase(2,2)]
        public void SurvivorCanReceiveWounds(int wounds, int expectedWounds)
        {
            var survivor = new Survivor("Sully");

            survivor.ReceiveWounds(wounds);

            Assert.That(survivor.Wounds, Is.EqualTo(expectedWounds));
        }

        [Test]
        [TestCase(2,true)]
        [TestCase(1,false)]
        public void SurvivorReceive2WoundsDie(int receivedWounds,bool expectedResoult)
        {
            var survivor = new Survivor("Wick");

            survivor.ReceiveWounds(2);

            Assert.That(survivor.IsDead, Is.EqualTo(true));
        }
        
        [Test]
        public void SurvivorReceivesMoreThan2WoundsAditionalWoundsAreIgnored()
        {
            var survivor = new Survivor("Kraken");

            survivor.ReceiveWounds(3);

            Assert.That(survivor.Wounds,Is.EqualTo(2));
        }
    }

    public class SurvivorEquipmentTests
    {
        [Test]
        public void SurvivorAddEquipmentToInventory()
        {
            var survivor = new Survivor("Bob");
            var targetEquipment = new Equipment("Baseball Bat");

            survivor.AddEquipment(targetEquipment);

            Assert.That(survivor.InReserve.Contains(targetEquipment), Is.EqualTo(true));
        }

        [Test]
        [TestCase (5,false)]
        [TestCase (4,true)]
        public void SurvivorCannotHaveMoreThan5Equipment(int itemsInReserve, bool expectedResult)
        {
            var survivor = new Survivor("Greedy");
            var targetEquipment = new Equipment("Pistol");

            for (int i = 0; i < itemsInReserve; i++)
            {
                survivor.AddEquipment(new Equipment("Equipment" + i));
            }
            survivor.AddEquipment(targetEquipment);

            Assert.That(survivor.InReserve.Contains(targetEquipment), Is.EqualTo(expectedResult));
        }

        [Test]
        public void SurvivorCanMoveReserveItemToHand()
        {
            var survivor = new Survivor("HandyMan");
            var targetEquipment = new Equipment("Spoon");

            survivor.AddEquipment(targetEquipment);
            survivor.MoveToHand(targetEquipment);

            Assert.That(survivor.InHand.Contains(targetEquipment),Is.EqualTo(true));
            Assert.That(survivor.InReserve.Contains(targetEquipment),Is.EqualTo(false));
        }

        [Test]
        public void SurvivorCannotMoveReserveItemToHandIfHave2ItemsInHand()
        {
            var survivor = new Survivor("HandyMan");
            var handEquipment1 = new Equipment("Knife");
            var HandEquipment2 = new Equipment("Fork");
            var targetEquipment = new Equipment("Spoon");

            //Equipments in Hand
            survivor.AddEquipment(handEquipment1);
            survivor.MoveToHand(handEquipment1);
            survivor.AddEquipment(HandEquipment2);
            survivor.MoveToHand(HandEquipment2);
            //------------

            survivor.AddEquipment(targetEquipment);
            survivor.MoveToHand(targetEquipment);

            Assert.That(survivor.InHand.Contains(targetEquipment), Is.EqualTo(false));
            Assert.That(survivor.InReserve.Contains(targetEquipment), Is.EqualTo(true));
        }

        [Test]
        public void SurvivorCannotAddEquipmentIf2InHandsAnd3InReserve()
        {
            var survivor = new Survivor("Pepe");
            var targetEquipment = new Equipment("Target");

            survivor.InHand.Add(new Equipment("HandGun"));
            survivor.InHand.Add(new Equipment("RailGun"));
            survivor.InReserve.Add(new Equipment("Reserve"));
            survivor.InReserve.Add(new Equipment("WaterBottle"));
            survivor.InReserve.Add(new Equipment("Exeed"));

            survivor.AddEquipment(targetEquipment);

            Assert.That(survivor.InReserve.Contains(targetEquipment), Is.EqualTo(false));
            Assert.That(survivor.InHand.Contains(targetEquipment), Is.EqualTo(false));
        }

        [Test]
        public void SurvivorCannotAddEquipmentIf1InHandsAnd4InReserve()
        {
            var survivor = new Survivor("Pepe");
            var targetEquipment = new Equipment("Target");
            survivor.InHand.Add(new Equipment("HandGun"));
            survivor.InReserve.Add(new Equipment("RailGun"));
            survivor.InReserve.Add(new Equipment("Reserve"));
            survivor.InReserve.Add(new Equipment("WaterBottle"));
            survivor.InReserve.Add(new Equipment("Exeed"));


            survivor.AddEquipment(targetEquipment);


            Assert.That(survivor.InReserve.Contains(targetEquipment), Is.EqualTo(false));
            Assert.That(survivor.InHand.Contains(targetEquipment), Is.EqualTo(false));

        }

        [Test]
        public void SurvivorLose1MaxEquipmentForEachWound()
        {
            var survivor = new Survivor("WoundedMan");

            survivor.ReceiveWounds(1);

            Assert.That(survivor.MaxEquipment,Is.EqualTo(4));
        }

        [Test]
        [TestCase(2,3,1)]
        [TestCase(1,4,1)]
        [TestCase(0,5,2)]
        public void SurvivorHasMoreEquipmentThanNewCapacityLoseEquipment(int HandEquipments,int ReserveEquipment, int wounds)
        {
            var survivor = new Survivor("DropMan");
            for (int i = 0; i < HandEquipments; i++)
            {
                var handEquipment = new Equipment("HandItem");
                survivor.AddEquipment(handEquipment);
                survivor.MoveToHand(handEquipment);
            }
            for (int i = 0; i < ReserveEquipment; i++)
            {
                var handEquipment = new Equipment("Reserve");
                survivor.AddEquipment(handEquipment);
            }

            survivor.ReceiveWounds(wounds);

            Assert.That(survivor.TotalEquipmentInPosesion(), Is.EqualTo(survivor.MaxEquipment));


        }
    }

    public class SurvivorLevelTests
    {
        [Test]
        public void SurvivorGainOneExperienceWhenKillZombie()
        {
            var survivor = new Survivor("Cage");

            survivor.KillZombie();

            Assert.That(survivor.Experience, Is.EqualTo(1));
        }

        [Test]
        [TestCase(6,Level.Blue)]
        [TestCase(7,Level.Yellow)]
        [TestCase(18,Level.Yellow)]
        [TestCase(19,Level.Orange)]
        [TestCase(42,Level.Orange)]
        [TestCase(43,Level.Red)]
        public void SurvivorChangeLevelDependingOnExperience(int killedZombies, Level expectedLevel)
        {
            var survivor = new Survivor("Lebron");

            for (int i = 0; i < killedZombies; i++)
            {
                survivor.KillZombie();
            }

            Assert.That(survivor.CurrentLevel, Is.EqualTo(expectedLevel));
        }
    }
}
