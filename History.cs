using System.Collections.Generic;

public class History
{
    public List<string> RecordedEvents = new List<string>();

    public void RecordEvent(string Event)
    {
        RecordedEvents.Add(Event);
    }
}